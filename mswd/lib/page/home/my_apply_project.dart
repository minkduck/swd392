import 'package:flutter/material.dart';
import 'package:mswd/utils/colors.dart';

class MyApplyProject extends StatefulWidget {
  const MyApplyProject({Key? key}) : super(key: key);

  @override
  State<MyApplyProject> createState() => _MyApplyProjectState();
}

class _MyApplyProjectState extends State<MyApplyProject> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Apply Project"),
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor: AppColors.mainColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [

          ],
        ),
      ),
    );
  }
}


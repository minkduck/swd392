import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:mswd/utils/app_layout.dart';
import 'package:mswd/utils/colors.dart';
import 'package:mswd/widgets/description_text.dart';

import '../../data/api/project/project_controller.dart';
import '../../data/api/project/project_repo.dart';
import '../../data/model/project/project_model.dart';
import '../../routes/route_helper.dart';
import '../../widgets/big_text.dart';
import '../../widgets/small_text.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}


class _HomeScreenState extends State<HomeScreen> {
  @override
  List<dynamic> projectlist = [];
  final ProjectController projectController = Get.put(ProjectController(projectRepo: Get.find()));

  Future<String> _loadData() async {
    // Simulate loading data for 5 seconds
    await Future.delayed(Duration(seconds: 2));
    return "Data loaded!";
  }
  @override
  void initState() {
    super.initState();
    projectController.getProjectList().then((result) {
      setState(() {
        projectlist = result;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Screen"),
        centerTitle: true,
        backgroundColor: AppColors.mainColor,
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            GetBuilder<ProjectController>(builder: (projects) {
              return projects.isLoaded
                  ?  ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: projectlist.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                       Get.toNamed(RouteHelper.getProject(index, "project"));
                    },

                    child: Container(
                      margin: EdgeInsets.only(
                          left: AppLayout.getWidth(20),
                          right: AppLayout.getWidth(20),
                          bottom:AppLayout.getHeight(20)),
                      child: Row(
                        children: [
                          //image section
                          Container(
                            width: AppLayout.getHeight(110),
                            height: AppLayout.getHeight(110),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                    AppLayout.getHeight(20)),
                                color: Colors.white38,
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  // image: NetworkImage("https://wowmart.vn/wp-content/uploads/2020/10/null-image.png"),)),
                      image: NetworkImage(projectlist[index]['image']!= null ? projectlist[index]['image']! : "https://wowmart.vn/wp-content/uploads/2020/10/null-image.png"),)),
                          ),
                          //text section
                          Expanded(
                            child: Container(
                              height: AppLayout.getHeight(100),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(
                                          AppLayout.getHeight(20)),
                                      bottomRight: Radius.circular(
                                          AppLayout.getHeight(20))),
                                  color: Colors.white),
                              child: Padding(
                                padding:
                                EdgeInsets.only(left: AppLayout.getHeight(20)),
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    BigText(
                                      text: projectlist[index]['project_name']!,
                                      // text: "hello",
                                      size: 18,
                                    ),
                                    Gap(AppLayout.getHeight(10)),
                                    DescriptionText(
                                        text: projectlist[index]['url'] != null ? projectlist[index]['url']! : "There is no info"
                                      // text: "hello"
                                    ),

                                    Gap(AppLayout.getHeight(10)),
                                    DescriptionText(
                                      text: projectlist[index]['description']!
                                      //   text: "hello"
                                    )
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              )
                  : CircularProgressIndicator();
            }),
          ],
        ),
      ),
    );
    return Scaffold();
  }
}

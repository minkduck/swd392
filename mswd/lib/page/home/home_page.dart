import 'package:flutter/material.dart';
import 'package:mswd/page/project/my_project.dart';


import '../../test/local_notifications.dart';
import '../../utils/colors.dart';
import '../account/account_page.dart';
import '../message/message_page.dart';
import 'my_apply_project.dart';
import 'home_screen.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectIndex = 0;
  List pages =[
    HomeScreen(),
    MyProject(),
    MyApplyProject(),
    Messagepage(),
    AccountPage()
  ];

  void onTapNav(int index){
    setState(() {
      _selectIndex = index;
    });
  }

  void initState() {
    // TODO: implement initState
    super.initState();

    LocalNotificationService.initilize();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages[_selectIndex],
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: AppColors.mainColor,
        unselectedItemColor: Colors.amberAccent,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        currentIndex: _selectIndex,
        onTap: onTapNav,
        items: const [
          BottomNavigationBarItem(
              icon: Icon(Icons.home_outlined,),
              label: "Home"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.insert_page_break_outlined,),
              label: "Proposal"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.request_page_outlined,),
              label: "Contract"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.message_outlined,),
              label: "Message"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person,),
              label: "Me"
          ),
        ],
      ),
    );
  }
}
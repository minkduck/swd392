import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/api/category/category_controller.dart';
import '../../data/api/major/major_controller.dart';
import '../../data/api/project/project_controller.dart';
import '../../data/api/student/student_controller.dart';
import '../../routes/route_helper.dart';
import '../../utils/snackbar_utils.dart';

class Messagepage extends StatefulWidget {
  const Messagepage({Key? key}) : super(key: key);

  @override
  State<Messagepage> createState() => _MessagepageState();
}
/*List<dynamic> _projectList = [];
List<dynamic> get projectList => _projectList;
Future<void> fetchProjects() async {
  final response = await http.get(Uri.parse('https://capstone-matching.herokuapp.com/api/v1/projects'), headers: {
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdHVkZW50X2lkIjoidnhRUVYyUElUT1JJakxDSjY2UkloVVdudTd1MiIsImVtYWlsIjoiZGg0MTE4NzI3QGdtYWlsLmNvbSIsInJvbGVfbmFtZSI6IkFkbWluIiwiaWF0IjoxNjc3NTAzNDU4LCJleHAiOjE2Nzc1MDcwNTh9.fOLFaKsCin_WIy99QwYjgfQ9xelOL3zpDktmHlx0U-Y'
  });

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON string into a list of Projects.
    final jsonData = json.decode(response.body);
    final List<Project> projects = (jsonData['projects']['rows'] as List).map((e) => Project.fromJson(e)).toList();
    _projectList.addAll(projects);
    // Do something with the list of projects
    print("prolist " + _projectList.toString());

    print(Projects.fromJson(jsonDecode(response.body)).rows);

  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load projects');
  }
}*/
class _MessagepageState extends State<Messagepage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Home page"),
        backgroundColor: Colors.amberAccent,
      ),
      body: Column(
        children: [
          Row(
            children: [
              ElevatedButton(
                  onPressed: () async {
                    final prefs = await SharedPreferences.getInstance();
                    print(prefs.getString('access_token'));
                    print(prefs.getString('student_id'));
                    Set<String> keys = prefs.getKeys();

                    for (String key in keys) {
                      print("$key: ${prefs.get(key)}");
                    }
                    // fetchProjects();
                    // Get.find<ProjectController>().getProjectList();
                    // Get.find<PostController>().getPostList();
                    // Get.find<StudentController>().getStudentList();
                    // Get.find<CategoryController>().getCategoryList();
                    // Get.find<MajorController>().getMajorList();
                    // Get.find<RoleController>().getRoleList();
                    // Get.find<StudentController>().getStudentById();
                    // Get.find<ProjectController>().getProjectPoster(1);
                    Get.toNamed(RouteHelper.getInitial());
                    Get.toNamed(RouteHelper.getInitial());
                    SnackbarUtils().showSuccess(title: "Successs", message: "Create new project successfully");
                  },
                  child: Text('Hello'))
            ],
          )
        ],
      ),
    );
  }
}

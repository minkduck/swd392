import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:mswd/data/api/auth/google_sign_in.dart';
import 'package:mswd/page/auth/sign_up_page.dart';
import 'package:mswd/test/login_widget.dart';
import 'package:provider/provider.dart';

import '../../utils/app_layout.dart';
import '../../utils/colors.dart';
import '../../widgets/app_text_field.dart';
import '../../widgets/big_text.dart';
import '../home/home_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    var emailController = TextEditingController();
    var passwordController = TextEditingController();

    List images = ["g.png", "t.png", "f.png"];

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            Gap(AppLayout.getScreenHeight() * 0.05),
            // app logo
            SizedBox(
              height: AppLayout.getScreenHeight() * 0.25,
              child: const Center(
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  radius: 80,
                  backgroundImage: AssetImage("assets/images/logo_1.jpg"),
                ),
              ),
            ),
            //welcome
            Container(
              margin: EdgeInsets.only(left: AppLayout.getWidth(20)),
              width: double.maxFinite,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Hello',
                    style: TextStyle(
                      fontSize: 50,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    'Sign into your account',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.grey[500],
                    ),
                  ),
                ],
              ),
            ),
            Gap(AppLayout.getScreenHeight() * 0.05),
            //your email
            AppTextField(
                textController: emailController,
                hintText: "Email",
                icon: Icons.email),
            Gap(AppLayout.getHeight(20)),
            //your password
            AppTextField(
                textController: passwordController,
                hintText: "Password",
                icon: Icons.password_sharp),
            Gap(AppLayout.getHeight(20)),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const Gap(10),
                Text(
                  'Forgot your password ?',
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
            Gap(AppLayout.getHeight(20)),
            Container(
              width: AppLayout.getScreenWidth() / 2,
              height: AppLayout.getScreenHeight() / 13,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(AppLayout.getHeight(30)),
                  color: AppColors.mainColor),
              child: ElevatedButton(
                onPressed: () {Navigator.push(
                    context, MaterialPageRoute(builder: (context) => LoggedInWidget()));},
                child: Center(
                  child: BigText(
                    text: "Log In",
                    size: AppLayout.getHeight(30),
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Gap(AppLayout.getHeight(20)),

            RichText(
                text: TextSpan(
                    text: "Don\'t have an account ?",
                    style: TextStyle(
                      color: Colors.grey[500],
                      fontSize: 20,
                    ),
                    children: [
                      TextSpan(
                          text: "  Create",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () => Get.to(() => const SignUpPage(),
                                transition: Transition.fade))
                    ])),
            Gap(AppLayout.getHeight(20)),
            Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: GestureDetector(
                      onTap: () {
                        final provider = Provider.of<GoogleSignInProvider>(context, listen: false);
                        provider.googleLogin();
                      },
                      child: CircleAvatar(
                        backgroundColor: Colors.grey[500],
                        radius: 30,
                        child: const CircleAvatar(
                          radius: 25,
                          backgroundImage:
                          AssetImage("assets/images/g.png"),
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}

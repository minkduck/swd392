import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:mswd/data/api/role/role_controller.dart';
import 'package:mswd/page/account/account_page.dart';
import 'package:mswd/page/auth/login_page.dart';
import 'package:mswd/test/login_widget.dart';
import 'package:mswd/page/home/home_page.dart';

import '../data/api/category/category_controller.dart';
import '../data/api/major/major_controller.dart';
import '../data/api/post/post_controller.dart';
import '../data/api/project/project_controller.dart';
import '../data/api/student/student_controller.dart';

class HomeLoginPage extends StatelessWidget {
  const HomeLoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Something Went Wrong!'),);
          } else if (snapshot.hasData) {
            Get.find<ProjectController>().getProjectList();
/*            Get.find<StudentController>().getStudentList();
            Get.find<CategoryController>().getCategoryList();
            Get.find<MajorController>().getMajorList();
            Get.find<RoleController>().getRoleList();*/
            return HomePage();
          } else {
            return LoginPage();
          }
        },
      ),
    );
  }
}

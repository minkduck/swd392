import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

import '../../utils/app_layout.dart';
import '../../utils/colors.dart';
import '../../widgets/account_widget.dart';
import '../../widgets/app_icon.dart';
import '../../widgets/big_text.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser!;

    return Scaffold(
      appBar: AppBar(
        title: BigText(text: "Profile",size: 24, color: Colors.white,),
        backgroundColor: AppColors.mainColor,
        centerTitle: true,
      ),
      body: Container(
        width: double.maxFinite,
        margin: EdgeInsets.only(top: AppLayout.getHeight(20)),
        child: Column(

          children: [
            //profile icon
            CircleAvatar(
              radius: 40,
              backgroundImage: NetworkImage(user.photoURL!),
            ),
            Gap(AppLayout.getHeight(30)),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    //name
                    AccountWidget(
                        appIcon: AppIcon(icon: Icons.person,
                          backgroundColor: AppColors.mainColor,
                          iconColor: Colors.white,
                          iconSize: AppLayout.getHeight(25),
                          size: AppLayout.getHeight(50),
                        ),
                        bigText: BigText(text: user.displayName!)
                    ),
                    Gap(AppLayout.getHeight(20)),
                    //phone
/*
                    AccountWidget(
                        appIcon: AppIcon(icon: Icons.phone,
                          backgroundColor: AppColors.yellowColor,
                          iconColor: Colors.white,
                          iconSize: AppLayout.getHeight(25),
                          size: AppLayout.getHeight(50),
                        ),
                        bigText: BigText(text: user.phoneNumber!)
                    ),
*/
                    Gap(AppLayout.getHeight(20)),
                    //email
                    AccountWidget(
                        appIcon: AppIcon(icon: Icons.email,
                          backgroundColor: AppColors.yellowColor,
                          iconColor: Colors.white,
                          iconSize: AppLayout.getHeight(25),
                          size: AppLayout.getHeight(50),
                        ),
                        bigText: BigText(text: user.email!)
                    ),
                    Gap(AppLayout.getHeight(20)),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

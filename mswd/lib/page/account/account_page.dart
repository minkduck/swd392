import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:mswd/data/api/firebase/upload_file_controller.dart';
import 'package:mswd/page/account/profile_page.dart';
import 'package:provider/provider.dart';

import '../../test/notification_push.dart';
import '../../test/upload_file.dart';
import '../../test/upload_file_backend.dart';
import '../../utils/app_layout.dart';
import '../../utils/colors.dart';
import '../../widgets/account_widget.dart';
import '../../widgets/app_icon.dart';
import '../../widgets/big_text.dart';
import '../../data/api/auth/google_sign_in.dart';

class AccountPage extends StatefulWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  final user = FirebaseAuth.instance.currentUser!;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile", style: TextStyle(color: Colors.grey),),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: AppLayout.getHeight(115),
                        width: AppLayout.getWidth(115),
                        child: Stack(
                          fit: StackFit.expand,
                          children: [
                            CircleAvatar(
                              radius: 40,
                              backgroundImage: NetworkImage(user.photoURL!),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Gap(AppLayout.getHeight(10)),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => ProfilePage()));
                },
                child: AccountWidget(appIcon: AppIcon(icon: Icons.person,
                  backgroundColor: AppColors.mainColor,
                  iconColor: Colors.white,
                  iconSize: AppLayout.getHeight(25),
                  size: AppLayout.getHeight(50),
                ), bigText: BigText(text: "Profile")),
              ),
              Gap(AppLayout.getHeight(15)),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => UploadFileScreen()));
                },
                child: AccountWidget(appIcon: AppIcon(icon: Icons.file_upload,
                  backgroundColor: Colors.green,
                  iconColor: Colors.white,
                  iconSize: AppLayout.getHeight(25),
                  size: AppLayout.getHeight(50),
                ), bigText: BigText(text: "Upload File Firebase ")),
              ),
              Gap(AppLayout.getHeight(15)),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => PushNotification()));
                },
                child: AccountWidget(appIcon: AppIcon(icon: Icons.file_upload,
                  backgroundColor: Colors.green,
                  iconColor: Colors.white,
                  iconSize: AppLayout.getHeight(25),
                  size: AppLayout.getHeight(50),
                ), bigText: BigText(text: "Push Notification")),
              ),
              Gap(AppLayout.getHeight(15)),
              GestureDetector(
                onTap: () {
                  final provider = Provider.of<GoogleSignInProvider>(context, listen: false);
                  provider.logout();
                },
                child: AccountWidget(appIcon: AppIcon(icon: Icons.logout,
                  backgroundColor: Colors.blue,
                  iconColor: Colors.white,
                  iconSize: AppLayout.getHeight(25),
                  size: AppLayout.getHeight(50),
                ), bigText: BigText(text: "Logout")),
              ),

            ],
          ),
        ),
      ),
    );
  }
}

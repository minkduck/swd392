import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mswd/data/api/category/category_controller.dart';
import 'package:mswd/data/api/firebase/upload_file_controller.dart';
import 'package:mswd/data/api/major/major_controller.dart';
import 'package:mswd/data/api/project/project_controller.dart';
import 'package:mswd/page/login_home_page.dart';
import 'package:mswd/utils/app_layout.dart';
import 'package:mswd/utils/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../data/model/project/project_model.dart';
import '../../routes/route_helper.dart';
import '../../utils/snackbar_utils.dart';

class CreateProjectPage extends StatefulWidget {
  const CreateProjectPage({Key? key}) : super(key: key);

  @override
  State<CreateProjectPage> createState() => _CreateProjectPageState();
}

class _CreateProjectPageState extends State<CreateProjectPage> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _budgetController = TextEditingController();
  final TextEditingController _urlController = TextEditingController();


  List<dynamic> catelist = [];
  final CategoryController categoryController = Get.put(CategoryController(categoryRepo: Get.find()));

  List<dynamic> majorist = [];
  final MajorController majorController = Get.put(MajorController(majorRepo: Get.find()));

  String? _selectedCateValue;
  String? _selectedMajorValue;

  final project = ProjectPost(
    projectName: "_titleController.text",
    description: "_descriptionController.text",
    price: "int.parse(_budgetController.text).toString()",
    image: "",
    url: "",
    categoryId: "_selectedCateValue",
    majorId: "_selectedMajorValue",
  );

  PlatformFile? pickedFIle;
  UploadTask? uploadTask;
  File? _imageFile;
  String? _imageUrl;
  late String token;

  Future selectFile() async {
    final result = await FilePicker.platform.pickFiles(allowMultiple: false);
    if (result == null) return;

    setState(() {
      pickedFIle = result.files.first;
    });
    Get.lazyPut(()=>UploadFileController());
    var url = Get.find<UploadFileController>().getUploadFile(pickedFIle!);
    print(url);
  }

  Future<void> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    token = prefs.getString('access_token')!;
    print(token);
  }

  Future<void> _uploadFile() async {
    await getToken();
    final url = Uri.parse('https://capstone-matching.herokuapp.com/api/v1/upload-file');

    final request = http.MultipartRequest('POST', url);
    request.headers.addAll({'Authorization': token});
    request.files.add(http.MultipartFile.fromBytes(
      'file',
      _imageFile!.readAsBytesSync(),
      filename: _imageFile!.path.split('/').last,
    ));

    final response = await http.Response.fromStream(await request.send());
    final imageUrl = jsonDecode(response.body)['url'];

    setState(() {
      _imageUrl = imageUrl;
    });
    print(_imageUrl);
  }

  Future<void> _pickImage() async {
    final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
      });
    }
    await _uploadFile();
  }

  @override
  void initState() {
    super.initState();
    _selectedCateValue = "";
    _selectedMajorValue = "";
    categoryController.getCategoryList().then((result) {
      setState(() {
        catelist = result;
      });
    });
    majorController.getMajorList().then((result) {
      setState(() {
        majorist = result;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create project'),
        backgroundColor: AppColors.mainColor,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                TextFormField(
                  controller: _titleController,
                  decoration: InputDecoration(
                    labelText: 'Project Name',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please input project name';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _descriptionController,
                  decoration: InputDecoration(
                    labelText: 'Description',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please input description';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _budgetController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Price',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty
                        || double.tryParse(value) == null) {
                      return 'Please input price';
                    }
                    return null;
                  },
                ),

                TextFormField(
                  controller: _urlController,
                  decoration: const InputDecoration(
                    labelText: 'Url',
                  ),
                ),

                Gap(AppLayout.getHeight(16)),
/*
                DropdownButtonFormField<String>(
                  value: _selectedValue,
                  onChanged: (value) {
                    setState(() {
                      _selectedValue = value.toString();
                    });
                  },
                  items: _dropdownItems,
                  decoration: const InputDecoration(
                    labelText: "Select your skill set",
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please select a skill set";
                    }
                    return null;
                  },
                ),
*/
                GetBuilder<CategoryController>(builder: (categories) {
                  return DropdownButtonFormField(
                    value: _selectedCateValue!.isNotEmpty ? _selectedCateValue : null,
                    items: [
                      DropdownMenuItem(child: Text(catelist[0]['cate_name']), value: catelist[0]['cate_id'],),
                      DropdownMenuItem(child: Text(catelist[1]['cate_name']), value: catelist[1]['cate_id'],),
                      DropdownMenuItem(child: Text(catelist[2]['cate_name']), value: catelist[2]['cate_id'],),
                    ],
                    onChanged: (value) {
                      setState(() {
                        _selectedCateValue = value.toString();
                      });
                    },
                    decoration: const InputDecoration(
                      labelText: "Select your skill set",
                    ),
                  );

                }),
                Gap(AppLayout.getHeight(16)),
                GetBuilder<MajorController>(builder: (majories) {
                  return DropdownButtonFormField(
                    value: _selectedMajorValue!.isNotEmpty ? _selectedMajorValue : null,
                    items: [
                      DropdownMenuItem(child: Text(majorist[0]['major_name']), value: majorist[0]['major_id'],),
                      DropdownMenuItem(child: Text(majorist[1]['major_name']), value: majorist[1]['major_id'],),
                      DropdownMenuItem(child: Text(majorist[2]['major_name']), value: majorist[2]['major_id'],),
                      DropdownMenuItem(child: Text(majorist[3]['major_name']), value: majorist[3]['major_id'],),
                      DropdownMenuItem(child: Text(majorist[4]['major_name']), value: majorist[4]['major_id'],),
                      DropdownMenuItem(child: Text(majorist[5]['major_name']), value: majorist[5]['major_id'],),
                      DropdownMenuItem(child: Text(majorist[6]['major_name']), value: majorist[6]['major_id'],),

                    ],
                    onChanged: (value) {
                      setState(() {
                        _selectedMajorValue = value.toString();
                      });
                    },
                    decoration: const InputDecoration(
                      labelText: "Select your major set",
                    ),
                  );

                }),

                Gap(AppLayout.getHeight(16)),
                Row(
                  children: [
                    ElevatedButton(
                      onPressed: _pickImage,
                      child: Text('Select Image'),
                    ),
                    if (_imageFile != null)
                      Expanded(
                        child: Container(
                          color: Colors.blue[100],
                          child: Image.file(
                            File(_imageFile!.path!),
                            width: double.infinity,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                  ],
                ),

                Gap(AppLayout.getHeight(16)),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      project.projectName = _titleController.text;
                      project.description = _descriptionController.text;
                      project.price = int.parse(_budgetController.text).toString();
                      project.categoryId = _selectedCateValue!;
                      project.majorId = _selectedMajorValue!;
                      project.image = _imageUrl!;
                      project.url = _urlController.text;
                      ProjectController(projectRepo: Get.find()).createProject(project);
                    }
                    SnackbarUtils().showSuccess(title: "Successs", message: "Create new project successfully");
                    Get.toNamed(RouteHelper.getInitial());
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(AppColors.mainColor),
                  ),
                  child: Text('Create project'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

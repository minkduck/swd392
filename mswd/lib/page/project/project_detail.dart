import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:mswd/data/api/application/application_controller.dart';
import 'package:mswd/utils/app_layout.dart';
import 'package:mswd/widgets/small_text.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../data/api/project/project_controller.dart';
import '../../routes/route_helper.dart';
import '../../utils/colors.dart';
import '../../utils/snackbar_utils.dart';
import '../../widgets/app_column.dart';
import '../../widgets/app_icon.dart';
import '../../widgets/big_text.dart';
import '../../widgets/exandable_text_widget.dart';

class ProjectDetail extends StatefulWidget {
  final int pageId;
  final String page;
  const ProjectDetail({Key? key, required this.pageId, required this.page}) : super(key: key);

  @override
  State<ProjectDetail> createState() => _ProjectDetailState();
}

class _ProjectDetailState extends State<ProjectDetail> {
  @override
  List<dynamic> projectlist = [];
  var posterlist;
  late String msg;
  late String token ;

  final ProjectController projectController = Get.put(ProjectController(projectRepo: Get.find()));
  late ApplicationController applicationController = Get.put(ApplicationController());

  Future<void> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    token = prefs.getString('access_token')!;
    print(token);
  }

  Future<void> getApplyProject(String projectId) async {
    await getToken();
    final Map<String, dynamic> requestBody = {
      'project_id': projectId,
    };

    // Encode the request body as JSON
    final requestBodyJson = jsonEncode(requestBody);

    // Define the API endpoint URL
    final url = Uri.parse(
        'https://capstone-matching.herokuapp.com/api/v1/applications');

    try {
      final response = await http.post(
        url,
        headers: {
          'Authorization': token,
          'Content-Type': 'application/json',
        },
        body: requestBodyJson,
      );

      if (response.statusCode == 200) {
        final url = jsonDecode(response.body)['msg'];
        if (url.toString() != "Apply successfully") {
          SnackbarUtils().showError(title: "Error", message: url.toString());
        } else {
          SnackbarUtils().showSuccess(title: "Successs", message: url.toString());
        }
        print('Application submitted successfully');
      } else {
        // Failed request
        print('Failed to submit application');
      }

    } catch (e) {
      // Handle any errors that occurred during the request
      print('Error occurred during application submission: $e');
    }
  }

  @override
  void initState() {
    super.initState();
    projectController.getProjectList().then((result) {
      setState(() {
        projectlist = result;
      });
    });
    projectController.getProjectPoster(widget.pageId).then((result) {
      setState(() {
        posterlist = result;
      });
    });

  }

  @override
  Widget build(BuildContext context) {
    // var project = Get.find<ProjectController>().projectList[widget.pageId];
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          //background image
          Positioned(
              left: 0,
              right: 0,
              child: Container(
                width: double.maxFinite,
                height: 250,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                      image: NetworkImage(projectlist[widget.pageId]['image']!= null ? projectlist[widget.pageId]['image']! : "https://wowmart.vn/wp-content/uploads/2020/10/null-image.png"),)),
              )),
          Positioned(
              top: AppLayout.getHeight(45),
              left: AppLayout.getWidth(20),
              right: AppLayout.getWidth(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                      onTap: () {
                          Get.toNamed(RouteHelper.getInitial());
                      },
                      child: const AppIcon(icon: Icons.arrow_back_ios)),
                ],
              )),

          Positioned(
              left: 0,
              right: 0,
              top: 250 - 20,
              child: Container(
                height: AppLayout.getHeight(300),
                padding: EdgeInsets.only(
                    left: AppLayout.getWidth(20),
                    right: AppLayout.getWidth(20),
                    top: AppLayout.getWidth(20)),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(AppLayout.getHeight(20)),
                        topLeft: Radius.circular(AppLayout.getHeight(20))),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BigText(
                      text: projectlist[widget.pageId]['project_name']!,
                      // text: "hello",
                      size: 25,
                    ),
                    Gap(AppLayout.getHeight(20)),
                    const Text("Introduce", style: TextStyle(fontSize: 15),),
                    Gap(AppLayout.getHeight(20)),
                    Row(
                      children: [
                        CircleAvatar(
                          radius: 20,
                          backgroundImage: NetworkImage(posterlist['avatar']!= null ? posterlist['avatar'] : "https://wowmart.vn/wp-content/uploads/2020/10/null-image.png"),
                        ),
                      Gap(AppLayout.getHeight(5)),
                      Text(posterlist['student_name'] != null ? posterlist['student_name'] : "This project was not created by anyone"),
                      ],
                    ),
                    Gap(AppLayout.getHeight(20)),
                    SmallText(text: projectlist[widget.pageId]['url']!),
                    Expanded(
                      child: SingleChildScrollView(
                          child:
                          ExpandableTextWidget(text: projectlist[widget.pageId]['description']!)),
                    ),
                  ],
                ),
              ))
          //expandable text widget
        ],
      ),
      bottomNavigationBar:
      Container(
          height: 100,
          padding: EdgeInsets.only(
              top: AppLayout.getHeight(30),
              bottom: AppLayout.getHeight(30),
              left: AppLayout.getWidth(30),
              right: AppLayout.getWidth(30)),
          decoration: BoxDecoration(
              color: AppColors.buttonBackgroundColor,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(AppLayout.getHeight(20) * 2),
                  topRight: Radius.circular(AppLayout.getHeight(20) * 2))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {
                  getApplyProject(projectlist[widget.pageId]['project_id']!);
                },
                child: BigText(text: "Apply", color: Colors.white,),
              ),
            ],
          ),
        ),
    );
  }
}
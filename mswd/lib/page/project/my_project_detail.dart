import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:mswd/data/api/application/application_controller.dart';
import 'package:mswd/page/project/my_project.dart';
import 'package:mswd/utils/app_layout.dart';
import 'package:mswd/widgets/small_text.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/api/project/project_controller.dart';
import '../../routes/route_helper.dart';
import '../../utils/colors.dart';
import '../../widgets/app_column.dart';
import '../../widgets/app_icon.dart';
import '../../widgets/big_text.dart';
import '../../widgets/exandable_text_widget.dart';

class MyProjectDetail extends StatefulWidget {
  final int pageId;
  final String page;

  const MyProjectDetail({Key? key, required this.pageId, required this.page})
      : super(key: key);

  @override
  State<MyProjectDetail> createState() => _MyProjectDetailState();
}

class _MyProjectDetailState extends State<MyProjectDetail> {
  late String posterId;

  @override
  List<dynamic> projectlist = [];
  final ProjectController projectController =
      Get.put(ProjectController(projectRepo: Get.find()));

  Future<void> getPosterId() async {
    final prefs = await SharedPreferences.getInstance();
    posterId = prefs.getString('student_id')!;
    print(posterId);
  }

  @override
  void initState() {
    super.initState();
    getPosterId().then((_) {
      projectController.getProjectListById(posterId).then((result) {
        setState(() {
          projectlist = result;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          //background image
          Positioned(
              left: 0,
              right: 0,
              child: Container(
                width: double.maxFinite,
                height: 250,
                decoration: BoxDecoration(
                    image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(projectlist[widget.pageId]['image'] !=
                          null
                      ? projectlist[widget.pageId]['image']!
                      : "https://wowmart.vn/wp-content/uploads/2020/10/null-image.png"),
                )),
              )),
          Positioned(
              top: AppLayout.getHeight(45),
              left: AppLayout.getWidth(20),
              right: AppLayout.getWidth(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: const AppIcon(icon: Icons.arrow_back_ios)),
                ],
              )),

          Positioned(
              left: 0,
              right: 0,
              top: 250 - 20,
              child: Container(
                height: AppLayout.getHeight(300),
                padding: EdgeInsets.only(
                    left: AppLayout.getWidth(20),
                    right: AppLayout.getWidth(20),
                    top: AppLayout.getWidth(20)),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(AppLayout.getHeight(20)),
                        topLeft: Radius.circular(AppLayout.getHeight(20))),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BigText(
                      text: projectlist[widget.pageId]['project_name']!,
                      // text: "hello",
                      size: 25,
                    ),
                    Gap(AppLayout.getHeight(20)),
                    const Text(
                      "Introduce",
                      style: TextStyle(fontSize: 15),
                    ),
                    Gap(AppLayout.getHeight(20)),
                    SmallText(text: projectlist[widget.pageId]['url']!),
                    Expanded(
                      child: SingleChildScrollView(
                          child: ExpandableTextWidget(
                              text: projectlist[widget.pageId]
                                  ['description']!)),
                    ),
                  ],
                ),
              ))
          //expandable text widget
        ],
      ),
      bottomNavigationBar: Container(
        height: 100,
        padding: EdgeInsets.only(
            top: AppLayout.getHeight(30),
            bottom: AppLayout.getHeight(30),
            left: AppLayout.getWidth(30),
            right: AppLayout.getWidth(30)),
        decoration: BoxDecoration(
            color: AppColors.buttonBackgroundColor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(AppLayout.getHeight(20) * 2),
                topRight: Radius.circular(AppLayout.getHeight(20) * 2))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                // applicationController.getApplyProject(projectlist[widget.pageId]['project_id']!);
              },
              child: BigText(
                text: "Apply",
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

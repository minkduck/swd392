import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:mswd/page/project/create_project.dart';
import 'package:mswd/utils/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/api/project/project_controller.dart';
import '../../routes/route_helper.dart';
import '../../utils/app_layout.dart';
import '../../widgets/big_text.dart';
import '../../widgets/description_text.dart';

class MyProject extends StatefulWidget {
  const MyProject({Key? key}) : super(key: key);

  @override
  State<MyProject> createState() => _MyProjectState();
}

class _MyProjectState extends State<MyProject> {
  late String posterId ;

  List<dynamic> projectlist = [];
  final ProjectController projectController = Get.put(ProjectController(projectRepo: Get.find()));

  Future<void> getPosterId() async {
    final prefs = await SharedPreferences.getInstance();
    posterId = prefs.getString('student_id')!;
    print(posterId);
  }
  @override
  void initState() {
    super.initState();
    getPosterId().then((_) {
      projectController.getProjectListById(posterId).then((result) {
        setState(() {
          projectlist = result;
        });
      });
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Project"),
        centerTitle: true,
        backgroundColor: AppColors.mainColor,
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            GetBuilder<ProjectController>(builder: (projects) {
              return projects.isLoaded
                  ?  ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: projectlist.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Get.toNamed(RouteHelper.getProjectById(index, "projectById"));
                    },

                    child: Container(
                      margin: EdgeInsets.only(
                          left: AppLayout.getWidth(20),
                          right: AppLayout.getWidth(20),
                          bottom:AppLayout.getHeight(20)),
                      child: Row(
                        children: [
                          //image section
                          Container(
                            width: AppLayout.getHeight(110),
                            height: AppLayout.getHeight(110),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                    AppLayout.getHeight(20)),
                                color: Colors.white38,
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  // image: NetworkImage("https://wowmart.vn/wp-content/uploads/2020/10/null-image.png"),)),
                                  image: NetworkImage(projectlist[index]['image']!= null ? projectlist[index]['image']! : "https://wowmart.vn/wp-content/uploads/2020/10/null-image.png"),)),
                          ),
                          //text section
                          Expanded(
                            child: Container(
                              height: AppLayout.getHeight(100),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(
                                          AppLayout.getHeight(20)),
                                      bottomRight: Radius.circular(
                                          AppLayout.getHeight(20))),
                                  color: Colors.white),
                              child: Padding(
                                padding:
                                EdgeInsets.only(left: AppLayout.getHeight(20)),
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    BigText(
                                      text: projectlist[index]['project_name']!,
                                      // text: "hello",
                                      size: 18,
                                    ),
                                    Gap(AppLayout.getHeight(10)),
                                    DescriptionText(
                                        text: projectlist[index]['url'] != null ? projectlist[index]['url']! : "There is no info"
                                      // text: "hello"
                                    ),

                                    Gap(AppLayout.getHeight(10)),
                                    DescriptionText(
                                        text: projectlist[index]['description']!
                                      //   text: "hello"
                                    )
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              )
                  : CircularProgressIndicator();
            }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => CreateProjectPage()));
        },
        tooltip: 'Create Project',
        backgroundColor: AppColors.mainColor,
        child: const Icon(Icons.add),
      ),

    );
  }
}

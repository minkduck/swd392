class Student {
  String? msg;
  Students? students;

  Student({this.msg, this.students});

  Student.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    students = json['students'] != null
        ? new Students.fromJson(json['students'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    if (this.students != null) {
      data['students'] = this.students!.toJson();
    }
    return data;
  }
}

class Students {
  int? count;
  List<Rows>? rows;

  Students({this.count, this.rows});

  Students.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['rows'] != null) {
      rows = <Rows>[];
      json['rows'].forEach((v) {
        rows!.add(new Rows.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.rows != null) {
      data['rows'] = this.rows!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Rows {
  String? studentId;
  String? studentName;
  String? email;
  String? avatar;
  String? status;
  String? createdAt;
  String? updatedAt;
  StudentRole? studentRole;
  StudentMajor? studentMajor;

  Rows(
      {this.studentId,
        this.studentName,
        this.email,
        this.avatar,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.studentRole,
        this.studentMajor});

  Rows.fromJson(Map<String, dynamic> json) {
    studentId = json['student_id'];
    studentName = json['student_name'];
    email = json['email'];
    avatar = json['avatar'];
    status = json['status'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    studentRole = json['student_role'] != null
        ? new StudentRole.fromJson(json['student_role'])
        : null;
    studentMajor = json['student_major'] != null
        ? new StudentMajor.fromJson(json['student_major'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['student_id'] = this.studentId;
    data['student_name'] = this.studentName;
    data['email'] = this.email;
    data['avatar'] = this.avatar;
    data['status'] = this.status;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    if (this.studentRole != null) {
      data['student_role'] = this.studentRole!.toJson();
    }
    if (this.studentMajor != null) {
      data['student_major'] = this.studentMajor!.toJson();
    }
    return data;
  }
}

class StudentRole {
  String? roleId;
  String? roleName;

  StudentRole({this.roleId, this.roleName});

  StudentRole.fromJson(Map<String, dynamic> json) {
    roleId = json['role_id'];
    roleName = json['role_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['role_id'] = this.roleId;
    data['role_name'] = this.roleName;
    return data;
  }
}

class StudentMajor {
  String? majorId;
  String? majorName;

  StudentMajor({this.majorId, this.majorName});

  StudentMajor.fromJson(Map<String, dynamic> json) {
    majorId = json['major_id'];
    majorName = json['major_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['major_id'] = this.majorId;
    data['major_name'] = this.majorName;
    return data;
  }
}
class Major {
  String? msg;
  Majors? majors;

  Major({this.msg, this.majors});

  Major.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    majors =
    json['majors'] != null ? new Majors.fromJson(json['majors']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    if (this.majors != null) {
      data['majors'] = this.majors!.toJson();
    }
    return data;
  }
}

class Majors {
  int? count;
  List<Rows>? rows;

  Majors({this.count, this.rows});

  Majors.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['rows'] != null) {
      rows = <Rows>[];
      json['rows'].forEach((v) {
        rows!.add(new Rows.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.rows != null) {
      data['rows'] = this.rows!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Rows {
  String? majorName;
  String? status;
  String? createdAt;
  String? updatedAt;

  Rows({this.majorName, this.status, this.createdAt, this.updatedAt});

  Rows.fromJson(Map<String, dynamic> json) {
    majorName = json['major_name'];
    status = json['status'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['major_name'] = this.majorName;
    data['status'] = this.status;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}
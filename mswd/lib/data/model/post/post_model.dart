class Post {
  String? msg;
  Posts? posts;

  Post({this.msg, this.posts});

  Post.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    posts = json['posts'] != null ? new Posts.fromJson(json['posts']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    if (this.posts != null) {
      data['posts'] = this.posts!.toJson();
    }
    return data;
  }
}

class Posts {
  int? count;
  List<Rows>? rows;

  Posts({this.count, this.rows});

  Posts.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['rows'] != null) {
      rows = <Rows>[];
      json['rows'].forEach((v) {
        rows!.add(new Rows.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.rows != null) {
      data['rows'] = this.rows!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Rows {
  String? postId;
  String? postTitle;
  String? description;
  String? timeStart;
  String? timeEnd;
  int? price;
  String? status;
  String? createdAt;
  String? updatedAt;
  PostCategory? postCategory;

  Rows(
      {this.postId,
        this.postTitle,
        this.description,
        this.timeStart,
        this.timeEnd,
        this.price,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.postCategory});

  Rows.fromJson(Map<String, dynamic> json) {
    postId = json['post_id'];
    postTitle = json['post_title'];
    description = json['description'];
    timeStart = json['time_start'];
    timeEnd = json['time_end'];
    price = json['price'];
    status = json['status'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    postCategory = json['post_category'] != null
        ? new PostCategory.fromJson(json['post_category'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this.postId;
    data['post_title'] = this.postTitle;
    data['description'] = this.description;
    data['time_start'] = this.timeStart;
    data['time_end'] = this.timeEnd;
    data['price'] = this.price;
    data['status'] = this.status;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    if (this.postCategory != null) {
      data['post_category'] = this.postCategory!.toJson();
    }
    return data;
  }
}

class PostCategory {
  String? cateId;
  String? cateName;

  PostCategory({this.cateId, this.cateName});

  PostCategory.fromJson(Map<String, dynamic> json) {
    cateId = json['cate_id'];
    cateName = json['cate_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cate_id'] = this.cateId;
    data['cate_name'] = this.cateName;
    return data;
  }
}
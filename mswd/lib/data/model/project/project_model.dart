class Project {
  Projects? projects;

  Project({this.projects});

  Project.fromJson(Map<String, dynamic> json) {
    projects = json['projects'] != null ? Projects.fromJson(json['projects']) : null;
  }
}

class Projects {
  int? count;
  List<Rows>? rows;

  Projects({this.count, this.rows});

  Projects.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['rows'] != null) {
      rows = <Rows>[];
      json['rows'].forEach((v) {
        rows!.add(new Rows.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.rows != null) {
      data['rows'] = this.rows!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Rows {
  String? projectId;
  String? projectName;
  String? description;
  int? price;
  String? url;
  String? image;
  String? status;
  ProjectPoster? projectPoster;
  ProjectPoster? projectDoer;
  ProjectCategory? projectCategory;
  ProjectMajor? projectMajor;

  Rows(
      {this.projectId,
        required this.projectName,
        required this.description,
        required this.price,
        this.url,
        required this.image,
        this.status,
        this.projectPoster,
        this.projectDoer,
        this.projectCategory,
        this.projectMajor});

  Rows.fromJson(Map<String, dynamic> json) {
    projectId = json['project_id'];
    projectName = json['project_name'];
    description = json['description'];
    price = json['price'];
    url = json['url'];
    image = json['image'];
    status = json['status'];
    projectPoster = json['project_poster'] != null
        ? new ProjectPoster.fromJson(json['project_poster'])
        : null;
    projectDoer = json['project_doer'] != null
        ? new ProjectPoster.fromJson(json['project_doer'])
        : null;
    projectCategory = json['project_category'] != null
        ? new ProjectCategory.fromJson(json['project_category'])
        : null;
    projectMajor = json['project_major'] != null
        ? new ProjectMajor.fromJson(json['project_major'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['project_id'] = this.projectId;
    data['project_name'] = this.projectName;
    data['description'] = this.description;
    data['price'] = this.price;
    data['url'] = this.url;
    data['image'] = this.image;
    data['status'] = this.status;
    if (this.projectPoster != null) {
      data['project_poster'] = this.projectPoster!.toJson();
    }
    if (this.projectDoer != null) {
      data['project_doer'] = this.projectDoer!.toJson();
    }
    if (this.projectCategory != null) {
      data['project_category'] = this.projectCategory!.toJson();
    }
    if (this.projectMajor != null) {
      data['project_major'] = this.projectMajor!.toJson();
    }
    return data;
  }
}

class ProjectPoster {
  String? studentId;
  String? studentName;
  String? avatar;

  ProjectPoster({this.studentId, this.studentName, this.avatar});

  ProjectPoster.fromJson(Map<String, dynamic> json) {
    studentId = json['student_id'];
    studentName = json['student_name'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['student_id'] = this.studentId;
    data['student_name'] = this.studentName;
    data['avatar'] = this.avatar;
    return data;
  }
}

class ProjectCategory {
  String? cateId;
  String? cateName;

  ProjectCategory({this.cateId, this.cateName});

  ProjectCategory.fromJson(Map<String, dynamic> json) {
    cateId = json['cate_id'];
    cateName = json['cate_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cate_id'] = this.cateId;
    data['cate_name'] = this.cateName;
    return data;
  }
}

class ProjectMajor {
  String? majorId;
  String? majorName;

  ProjectMajor({this.majorId, this.majorName});

  ProjectMajor.fromJson(Map<String, dynamic> json) {
    majorId = json['major_id'];
    majorName = json['major_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['major_id'] = this.majorId;
    data['major_name'] = this.majorName;
    return data;
  }
}

class ProjectPost {
  String projectName;
  String description;
  String price;
  String image;
  String url;
  String categoryId;
  String majorId;

  ProjectPost({
    required this.projectName,
    required this.description,
    required this.price,
    required this.image,
    required this.url,
    required this.categoryId,
    required this.majorId,
  });

  Map<String, dynamic> toJson() {
    return {
      'project_name': projectName,
      'description': description,
      'price': price,
      'image': image,
      'url': url,
      'cate_id': categoryId,
      'major_id': majorId,
    };
  }
}

import 'package:flutter/material.dart';
import 'package:mswd/utils/app_constraints.dart';

import '../api_clients.dart';
import 'package:get/get.dart';

class RoleRepo extends GetxService{
  late ApiClient apiClient;
  RoleRepo({required this.apiClient});

  Future<Response> getRoleList() async {
    return await apiClient.getData(AppConstrants.PROJECT_URL);
  }
}
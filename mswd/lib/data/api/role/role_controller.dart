import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import 'package:mswd/data/api/role/role_repo.dart';
import 'package:mswd/data/model/role/role_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RoleController extends GetxController{
  late RoleRepo roleRepo;
  RoleController({required this.roleRepo});
  late String token ;

  List<dynamic> _roleList = [];
  List<dynamic> get roleList => _roleList;

  bool _isLoaded = false;
  bool get isLoaded => _isLoaded;

  Future<void> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    token = prefs.getString('access_token')!;
    print(token);
  }

  Future<void> getRoleList() async {
    await getToken();
    final response = await http.get(Uri.parse('https://capstone-matching.herokuapp.com/api/v1/roles'), headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': token
    });
    if (response.statusCode == 200) {
      print("got roles");
      final jsonData = json.decode(response.body);
      final List<Role> projects = (jsonData['roles']['rows'] as List).map((e) => Role.fromJson(e)).toList();
      _roleList.addAll(projects);
      _isLoaded = true;
      update();
      print("roleslist " + _roleList.toString());
    } else {
      print(response.statusCode);
      throw Exception('Failed to load projects');
    }
  }

}
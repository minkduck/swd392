import 'package:shared_preferences/shared_preferences.dart';

// Lưu vai trò của người dùng vào bộ nhớ cục bộ
Future<void> saveUserRole(String role) async {
  final prefs = await SharedPreferences.getInstance();
  await prefs.setString('userRole', role);
}

// Lấy vai trò của người dùng từ bộ nhớ cục bộ
Future<String?> getUserRole() async {
  final prefs = await SharedPreferences.getInstance();
  return prefs.getString('userRole');
}

// Xóa vai trò của người dùng khỏi bộ nhớ cục bộ
Future<void> removeUserRole() async {
  final prefs = await SharedPreferences.getInstance();
  await prefs.remove('userRole');
}

import 'dart:convert';

import 'package:get/get.dart';
import 'package:mswd/data/api/post/post_repo.dart';
import 'package:mswd/data/model/post/post_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class PostController extends GetxController{
  late PostRepo postRepo;
  PostController({required this.postRepo});

  late String token ;

  List<dynamic> _postList = [];
  List<dynamic> get postList => _postList;

  bool _isLoaded = false;
  bool get isLoaded => _isLoaded;

  Future<void> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    token = prefs.getString('access_token')!;
    print(token);
  }

  Future<void> getPostList() async {
    await getToken();
    final response = await http.get(Uri.parse('https://capstone-matching.herokuapp.com/api/v1/posts'), headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': token
    });
    if (response.statusCode == 200) {
      print("got posts");
      final jsonData = json.decode(response.body);
      final List<Post> posts = (jsonData['posts']['rows'] as List).map((e) => Post.fromJson(e)).toList();
      _postList.addAll(posts);
      _isLoaded = true;
      update();
      print("postlist " + _postList.toString());
    } else {
      print(response.statusCode);
      throw Exception('Failed to load projects');
    }
  }
}
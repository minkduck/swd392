import 'package:flutter/material.dart';
import 'package:mswd/utils/app_constraints.dart';

import '../api_clients.dart';
import 'package:get/get.dart';

class PostRepo extends GetxService{
  late ApiClient apiClient;
  PostRepo({required this.apiClient});

  Future<Response> getPostList() async {
    return await apiClient.getData(AppConstrants.POST_URL);
  }
}
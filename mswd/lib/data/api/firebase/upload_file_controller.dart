import 'dart:convert';
import 'package:file_picker/file_picker.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class UploadFileController extends GetxController{
  late String token;

  bool _isLoaded = false;
  bool get isLoaded => _isLoaded;

  Future<void> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    token = prefs.getString('access_token')!;
    print(token);
  }

  Future<List<dynamic>> getUploadFile(PlatformFile file) async {
    await getToken();
    final response = await http.post(Uri.parse('https://capstone-matching.herokuapp.com/api/v1/upload-file'), headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': token
    }, body: file);
    if (response.statusCode == 200) {
      print("got file");
      final url = jsonDecode(response.body);

      _isLoaded = true;
      update();
      return url;
    } else {
      print(response.statusCode);
      throw Exception('Failed to load projects');
    }
  }

  Future<String> uploadFile(String filePath) async {
    await getToken();
    var request = http.MultipartRequest('POST', Uri.parse('https://capstone-matching.herokuapp.com/api/v1/upload-file'));
    request.headers.addAll({'authorization': token});
    request.files.add(await http.MultipartFile.fromPath('file', filePath));
    var response = await request.send();
    if (response.statusCode == 200) {
      return json.decode(await response.stream.bytesToString())['url'];
    } else {
      throw Exception('Failed to upload file');
    }
  }
}
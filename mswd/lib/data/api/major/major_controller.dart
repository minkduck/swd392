import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import 'package:mswd/data/api/major/major_repo.dart';
import 'package:mswd/data/model/major/major_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MajorController extends GetxController{
  late MajorRepo majorRepo;
  MajorController({required this.majorRepo});
  late String token ;

  List<dynamic> _majorList = [];
  List<dynamic> get majorList => _majorList;

  bool _isLoaded = false;
  bool get isLoaded => _isLoaded;

  Future<void> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    token = prefs.getString('access_token')!;
    print(token);
  }

  Future<List<dynamic>> getMajorList() async {
    await getToken();
    final response = await http.get(Uri.parse('https://capstone-matching.herokuapp.com/api/v1/majors'), headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': token
    });
    if (response.statusCode == 200) {
      print("got major");
      final majors = jsonDecode(response.body)['majors']['rows'];
      print("major: " + majors.toString());
      _isLoaded = true;
      update();
      return majors;
    } else {
      print(response.statusCode);
      throw Exception('Failed to load projects');
    }
  }

}
import 'package:flutter/material.dart';
import 'package:mswd/utils/app_constraints.dart';

import '../api_clients.dart';
import 'package:get/get.dart';

class MajorRepo extends GetxService{
  late ApiClient apiClient;
  MajorRepo({required this.apiClient});

  Future<Response> getMajorList() async {
    return await apiClient.getData(AppConstrants.MAJOR_URL);
  }
}
import 'dart:convert';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class GoogleSignInProvider extends ChangeNotifier {
  final googleSignIn = GoogleSignIn();

  bool _isAuthenticated = false;

  bool get isAuthenticated => _isAuthenticated;

  GoogleSignInAccount? _user;

  GoogleSignInAccount get user => _user!;

  final FirebaseAuth _auth = FirebaseAuth.instance;

  late String _accessToken;
  late String _studentRole;
  late String _studentId;

  Future googleLogin() async {
    await Firebase.initializeApp();

    try {
      final GoogleSignInAccount? googleUser = await googleSignIn.signIn();
      if (googleUser == null) return;
      _user = googleUser;

      final googleAuth = await googleUser.authentication;

      final OAuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      final UserCredential userCredential = await _auth.signInWithCredential(credential);

      final idToken = await userCredential.user!.getIdToken();
      print(" id token:$idToken");

      final response = await http.post(
        Uri.parse('https://capstone-matching.herokuapp.com/api/v1/auth/login-google'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $idToken',
        },
        body: jsonEncode(<String, String>{
          'idToken': idToken,
        }),
      );

      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
        _accessToken = responseData['access_token'];

        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('access_token', _accessToken);

        print("Access token" + _accessToken);

        if (response.statusCode == 200) {
          final userResponseData = jsonDecode(response.body);
          _studentRole = userResponseData['student']['student_role']['role_name'];
          _studentId = userResponseData['student']['student_id'];

          // print(_studentRole);

          await prefs.setString('student_role', _studentRole);
          await prefs.setString("student_id", _studentId);
        }

        print(prefs.getString('access_token'));
        print(prefs.getString('student_id'));

        // print(" respones.body: " + response.body);
        return response.body;
      } else {
        throw Exception('Failed to login with Google.');
      }
    } catch (e) {
     print(e.toString());
    }

    notifyListeners();
  }

  Future<void> logout() async {
    await googleSignIn.signOut();
    await _auth.signOut();
    _isAuthenticated = false;
    notifyListeners();
  }
  
}
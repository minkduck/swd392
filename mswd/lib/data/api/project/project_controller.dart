import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mswd/data/api/project/project_repo.dart';
import 'package:mswd/data/model/project/project_model.dart';
import 'package:http/http.dart' as http;

import '../../../routes/route_helper.dart';
import '../../../utils/snackbar_utils.dart';

class ProjectController extends GetxController{
  late ProjectRepo projectRepo;
  ProjectController({required this.projectRepo});
  late String token ;

  late final List<dynamic> _projectList = [];
  List<dynamic> get projectList => _projectList;

  bool _isLoaded = false;
  bool get isLoaded => _isLoaded;

/*
  Future<void> getProjectList() async {
    Response response = await projectRepo.getProjectList();
    if(response.statusCode == 200) {
      print("got projects");
      _projectList = [];
      final jsonData = json.decode(response.body);
      final List<Project> projects = (jsonData['projects']['rows'] as List).map((e) => Project.fromJson(e)).toList();
      _projectList.addAll(projects);
      _isLoaded = true;
      update();
      print(_projectList);
    } else {
      print(response.statusCode);
    }
    }
*/
  Future<void> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    token = prefs.getString('access_token')!;
    print(token);
  }

  Future<List<dynamic>>  getProjectList() async {
    await getToken();
    final response = await http.get(Uri.parse('https://capstone-matching.herokuapp.com/api/v1/projects'), headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': token
    });
    if (response.statusCode == 200) {
      print("got projects");
      final projects = jsonDecode(response.body)['projects']['rows'];
      _isLoaded = true;
      update();
      return projects;
    } else {
      print(response.statusCode);
      throw Exception('Failed to load projects');
    }
  }

  Future<List<dynamic>>  getProjectListById(String posterId) async {
    await getToken();
    final response = await http.get(Uri.parse('https://capstone-matching.herokuapp.com/api/v1/projects?poster_id=$posterId'), headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': token
    });
    if (response.statusCode == 200) {
      print("got projects");
      final projects = jsonDecode(response.body)['projects']['rows'];
      _isLoaded = true;
      update();
      return projects;
    } else {
      print(response.statusCode);
      throw Exception('Failed to load projects');
    }
  }


  Future<dynamic>  getProjectPoster(int index) async {
    await getToken();
    final response = await http.get(Uri.parse('https://capstone-matching.herokuapp.com/api/v1/projects'), headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': token
    });
    if (response.statusCode == 200) {
      print("got projects poster");
      final poster = jsonDecode(response.body)['projects']['rows'][index]['project_poster'];
      print(poster);
      _isLoaded = true;
      update();
      return poster;
    } else {
      print(response.statusCode);
      throw Exception('Failed to load projects');
    }
  }

  Future<void> createProject(ProjectPost project) async {
    final url = 'https://capstone-matching.herokuapp.com/api/v1/projects';
    await getToken();
    final response = await http.post(
      Uri.parse(url),
      headers: {'Authorization': token},
      body: project.toJson(),
    );

    if (response.statusCode == 200) {
      final msg = jsonDecode(response.body);
      if (msg == "Create new project successfully") {
        // SnackbarUtils().showSuccess(title: "Successs", message: "Create new project successfully");
        // Get.toNamed(RouteHelper.getInitial());
      }
    } else {
      print(response.statusCode);
    }
  }

}
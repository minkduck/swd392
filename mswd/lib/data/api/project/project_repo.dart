import 'package:flutter/material.dart';
import 'package:mswd/utils/app_constraints.dart';

import '../api_clients.dart';
import 'package:get/get.dart';

class ProjectRepo extends GetxService{
  late ApiClient apiClient;
  ProjectRepo({required this.apiClient});
  
  Future<Response> getProjectList() async {
    return await apiClient.getData(AppConstrants.PROJECT_URL);
  }
}
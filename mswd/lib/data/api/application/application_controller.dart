import 'dart:convert';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../../utils/snackbar_utils.dart';

class ApplicationController extends GetxController {
  late String token;

  bool _isLoaded = false;

  bool get isLoaded => _isLoaded;

  Future<void> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    token = prefs.getString('access_token')!;
    print(token);
  }

  Future<void> getApplyProject(String projectId) async {
    await getToken();
    final Map<String, dynamic> requestBody = {
      'project_id': projectId,
    };

    // Encode the request body as JSON
    final requestBodyJson = jsonEncode(requestBody);

    // Define the API endpoint URL
    final url = Uri.parse(
        'https://capstone-matching.herokuapp.com/api/v1/applications');

    try {
      final response = await http.post(
        url,
        headers: {
          'Authorization': token,
          'Content-Type': 'application/json',
        },
        body: requestBodyJson,
      );

      if (response.statusCode == 200) {
        final url = jsonDecode(response.body);
        print(url);
        SnackbarUtils().showSuccess(title: "Successs", message: url.toString());
        print('Application submitted successfully');
      } else {
        // Failed request
        print('Failed to submit application');
      }

    } catch (e) {
      // Handle any errors that occurred during the request
      print('Error occurred during application submission: $e');
    }
  }
}
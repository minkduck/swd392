import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import 'package:mswd/data/api/category/category_repo.dart';
import 'package:mswd/data/model/category/category_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CategoryController extends GetxController{
  late CategoryRepo categoryRepo;
  CategoryController({required this.categoryRepo});

  late String token ;

  List<dynamic> _categoryList = [];
  List<dynamic> get categoryList => _categoryList;

  bool _isLoaded = false;
  bool get isLoaded => _isLoaded;

  Future<void> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    token = prefs.getString('access_token')!;
    print(token);
  }

  Future<List<dynamic>> getCategoryList() async {
    await getToken();
    final response = await http.get(Uri.parse('https://capstone-matching.herokuapp.com/api/v1/categories'), headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': token
    });
    if (response.statusCode == 200) {
      print("got category");
      final categories = jsonDecode(response.body)['categories']['rows'];
      print("categories: $categories");
      _isLoaded = true;
      update();
      return categories;
    } else {
      print(response.statusCode);
      throw Exception('Failed to load projects');
    }
  }

}
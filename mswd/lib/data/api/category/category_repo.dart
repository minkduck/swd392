import 'package:flutter/material.dart';
import 'package:mswd/utils/app_constraints.dart';

import '../api_clients.dart';
import 'package:get/get.dart';

class CategoryRepo extends GetxService{
  late ApiClient apiClient;
  CategoryRepo({required this.apiClient});

  Future<Response> getCategoryList() async {
    return await apiClient.getData(AppConstrants.CATEGORY_URL);
  }
}
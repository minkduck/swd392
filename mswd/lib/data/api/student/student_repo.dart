import 'package:mswd/utils/app_constraints.dart';

import '../api_clients.dart';
import 'package:get/get.dart';

class StudentRepo extends GetxService{
  late ApiClient apiClient;
  StudentRepo({required this.apiClient});

  Future<Response> getStudentList() async {
    return await apiClient.getData(AppConstrants.STUDENT_URL);
  }
}
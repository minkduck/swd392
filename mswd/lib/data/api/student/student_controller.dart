import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import 'package:mswd/data/api/student/student_repo.dart';
import 'package:mswd/data/model/student/student_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StudentController extends GetxController{
  late StudentRepo studentRepo;
  StudentController({required this.studentRepo});
  late String token;
  late String tokenStudent;

  List<dynamic> _studentList = [];
  List<dynamic> get studentList => _studentList;

  bool _isLoaded = false;
  bool get isLoaded => _isLoaded;

  Future<void> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    token = prefs.getString('access_token')!;
    print(token);
  }

  Future<void> getStudentId() async {
    final prefs = await SharedPreferences.getInstance();
    tokenStudent = prefs.getString('student_id')!;
    print(tokenStudent);
  }

  Future<void> getStudentList() async {
    await getToken();
    final response = await http.get(Uri.parse('https://capstone-matching.herokuapp.com/api/v1/students'), headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': token
    });
    if (response.statusCode == 200) {
      print("got students");
      final students = jsonDecode(response.body)['student']['rows'];
      _isLoaded = true;
      update();
      return students;
    } else {
      print(response.statusCode);
      throw Exception('Failed to load projects');
    }
  }

  Future<void> getStudentById() async {
    await getToken();
    await getStudentId();
    final response = await http.get(Uri.parse('https://capstone-matching.herokuapp.com/api/v1/students/${tokenStudent}'), headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': token
    });
    if (response.statusCode == 200) {
      print("got student by id");
      final student = jsonDecode(response.body)['student'];
      print("student: " + jsonEncode(student));
      _isLoaded = true;
      update();
      return student;
    } else {
      print(response.statusCode);
      throw Exception('Failed to load projects');    }
  }
}
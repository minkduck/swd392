import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UploadFileScreen extends StatefulWidget {
  @override
  _UploadFileScreenState createState() => _UploadFileScreenState();
}

class _UploadFileScreenState extends State<UploadFileScreen> {
  File? _imageFile;
  String? _imageUrl;
  late String token;

  Future<void> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    token = prefs.getString('access_token')!;
    print(token);
  }
  Future<void> _uploadFile() async {
    await getToken();
    final url = Uri.parse('https://capstone-matching.herokuapp.com/api/v1/upload-file');

    final request = http.MultipartRequest('POST', url);
    request.headers.addAll({'Authorization': token});
    request.files.add(http.MultipartFile.fromBytes(
      'file',
      _imageFile!.readAsBytesSync(),
      filename: _imageFile!.path.split('/').last,
    ));

    final response = await http.Response.fromStream(await request.send());
    final imageUrl = response.body;

    setState(() {
      _imageUrl = imageUrl;
    });
    print(imageUrl);
  }

  Future<void> _pickImage() async {
    final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
      });
    }
    await _uploadFile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Upload File'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (_imageUrl != null)
              Image.network(
                _imageUrl!,
                height: 300,
              ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: _pickImage,
              child: Text('Pick Image'),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: _imageFile == null ? null : _uploadFile,
              child: Text('Upload File'),
            ),
          ],
        ),
      ),
    );
  }
}

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class MyWidget extends StatelessWidget {
  final String token;
  final int projectId;

  MyWidget({required this.token, required this.projectId});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () async {
        // Define the request body as a Map object
        final Map<String, dynamic> requestBody = {
          'project_id': projectId,
        };

        // Encode the request body as JSON
        final requestBodyJson = jsonEncode(requestBody);

        // Define the API endpoint URL
        final url = Uri.parse('https://capstone-matching.herokuapp.com/api/v1/applications');

        try {
          // Make the POST request to the API endpoint
          final response = await http.post(
            url,
            headers: {
              'Authorization': 'Bearer $token', // Pass the authorization token in the headers
              'Content-Type': 'application/json', // Set the content type to JSON
            },
            body: requestBodyJson, // Pass the encoded request body
          );

          // Handle the response based on its status code
          if (response.statusCode == 200) {
            // Successful request
            print('Application submitted successfully');
          } else {
            // Failed request
            print('Failed to submit application');
          }
        } catch (e) {
          // Handle any errors that occurred during the request
          print('Error occurred during application submission: $e');
        }
      },
      child: Text('Submit Application'),
    );
  }
}

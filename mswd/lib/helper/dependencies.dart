import 'package:mswd/data/api/category/category_controller.dart';
import 'package:mswd/data/api/major/major_controller.dart';
import 'package:mswd/data/api/major/major_repo.dart';
import 'package:mswd/data/api/project/project_controller.dart';
import 'package:mswd/data/api/role/role_controller.dart';
import 'package:mswd/data/api/role/role_repo.dart';
import 'package:mswd/data/model/category/category_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get.dart';

import '../data/api/api_clients.dart';
import '../data/api/category/category_repo.dart';
import '../data/api/post/post_controller.dart';
import '../data/api/post/post_repo.dart';
import '../data/api/project/project_repo.dart';
import '../data/api/student/student_controller.dart';
import '../data/api/student/student_repo.dart';
import '../utils/app_constraints.dart';


Future<void> init() async {
  final sharedPreferences = await SharedPreferences.getInstance();

  Get.lazyPut(() => sharedPreferences);
  //api client
  Get.lazyPut(() => ApiClient(appBaseUrl: AppConstrants.BASE_URL));

  //project
  Get.lazyPut(() => ProjectRepo(apiClient: Get.find()));
  Get.lazyPut(() => ProjectController(projectRepo: Get.find()));

  //post
  Get.lazyPut(() => PostRepo(apiClient: Get.find()));
  Get.lazyPut(() => PostController(postRepo: Get.find()));

  //students
  Get.lazyPut(() => StudentRepo(apiClient: Get.find()));
  Get.lazyPut(() => StudentController(studentRepo: Get.find()));

  //category
  Get.lazyPut(() => CategoryRepo(apiClient: Get.find()));
  Get.lazyPut(() => CategoryController(categoryRepo: Get.find()));

  //major
  Get.lazyPut(() => MajorRepo(apiClient: Get.find()));
  Get.lazyPut(() => MajorController(majorRepo: Get.find()));

  //role
  Get.lazyPut(() => RoleRepo(apiClient: Get.find()));
  Get.lazyPut(() => RoleController(roleRepo: Get.find()));
}

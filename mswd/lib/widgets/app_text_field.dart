import 'package:flutter/material.dart';
import 'package:mswd/utils/app_layout.dart';

import '../utils/colors.dart';

class AppTextField extends StatelessWidget {
  late var textController;
  final String hintText;
  final IconData icon;

  AppTextField(
      {Key? key,
      required this.textController,
      required this.hintText,
      required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          left: AppLayout.getHeight(20), right: AppLayout.getHeight(20)),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(AppLayout.getHeight(30)),
          boxShadow: [
            BoxShadow(
                blurRadius: 10,
                spreadRadius: 7,
                offset: Offset(1, 10),
                color: Colors.grey.withOpacity(0.2))
          ]),
      child: TextFormField(
        onSaved: (value) => textController = value,
        controller: textController,
        decoration: InputDecoration(
            hintText: hintText,
            prefixIcon: Icon(
              icon,
              color: AppColors.yellowColor,
            ),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(AppLayout.getHeight(30)),
                borderSide: BorderSide(
                  width: 1.0,
                  color: Colors.white,
                )),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(AppLayout.getHeight(30)),
                borderSide: BorderSide(width: 1.0, color: Colors.white))),
      ),
    );
  }
}

import 'package:get/get.dart';
import 'package:mswd/page/login_home_page.dart';
import 'package:mswd/page/project/my_project_detail.dart';
import 'package:mswd/page/project/project_detail.dart';

import '../page/home/home_screen.dart';

class RouteHelper{
  static const String splashPage = "/splash-page";
  static const String initial = "/";
  static const String project = "/project";
  static const String projectById = "/projectById";


  static String getInitial() => '$initial' ;
  static String getProject(int pageId, String page) => '$project?pageId=$pageId&page=$page';
  static String getProjectById(int pageId, String page) => '$projectById?pageId=$pageId&page=$page';

  static List<GetPage> routes = [
    GetPage(name: initial, page: () => const HomeLoginPage()),
    GetPage(name: project, page: () {
      var pageId = Get.parameters['pageId'];
      var page = Get.parameters['page'];
      return ProjectDetail(pageId: int.parse(pageId!), page: page!);
    },
        transition: Transition.fadeIn
    ),
    GetPage(name: projectById, page: () {
      var pageId = Get.parameters['pageId'];
      var page = Get.parameters['page'];
      return MyProjectDetail(pageId: int.parse(pageId!), page: page!);
    },
        transition: Transition.fadeIn
    ),

  ];

}
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:mswd/data/api/auth/google_sign_in.dart';
import 'package:mswd/data/api/project/project_controller.dart';
import 'package:mswd/page/project/create_project.dart';
import 'package:mswd/page/login_home_page.dart';
import 'package:mswd/page/auth/login_page.dart';
import 'package:mswd/page/home/home_page.dart';
import 'package:mswd/page/message/message_page.dart';
import 'package:mswd/page/project/project_detail.dart';
import 'package:mswd/routes/route_helper.dart';
import 'package:mswd/test/local_notifications.dart';
import 'package:mswd/test/notification_push.dart';
import 'package:provider/provider.dart';
import 'helper/dependencies.dart' as dep;

Future<void> backgroundHandler(RemoteMessage message) async {
  print("This is message from background");
  print(message.notification!.title);
  print(message.notification!.body);
}

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await dep.init();

  FirebaseMessaging.onBackgroundMessage(backgroundHandler);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

/*  @override
  void initState() {
    LocalNotificationService.initilize();
  }*/

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) => GoogleSignInProvider(),
        child: GetMaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'mswd',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: HomeLoginPage(),
          getPages: RouteHelper.routes,

        ));
  }
}
